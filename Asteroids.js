var lastTime = null;
var canvas = document.querySelector("canvas")
var cx = canvas.getContext("2d");
var controls = {87:"up", 83:"down", 65:"left", 68:"right", 32:"fire", 27:"pause"};
var asteroidSize = 25;
var asteroidLives = 4;
var asteroidSpeed = 80;
var maxStep = .01;

function control(){
	//create object of pressed values
	var pressed = Object.create(null);
	function handler(event){
		if(controls[event.keyCode]){
			//pause only triggers on keydown
			if(controls[event.keyCode] == "pause"){
              if(event.type == "keydown" && pressed[event.keyCode] === undefined){
              	pressed[event.keyCode] = false;
              }
				if(event.type == "keydown" && pressed[event.keyCode] == false){
					pressed[event.keyCode] = true;
				}else if(event.type == "keydown" && pressed[event.keyCode] == true){
					pressed[event.keyCode] = false;
				}
			}
			//puts held down in 
			else{
				var down = event.type == "keydown";
				if(controls[event.keyCode] !==null){
					pressed[event.keyCode] = down;
				}
				event.preventDefault();
			}
		}
	}
	addEventListener("keydown", handler);
	addEventListener("keyup", handler);
	return pressed;
}
{function Vector(x, y){
	this.x = x;
	this.y = y;
}
Vector.prototype.plus = function(plusVector){
	return new Vector(this.x + plusVector.x, this.y + plusVector.y)
}
Vector.prototype.scale = function(scale){
	return new Vector(this.x*scale, this.y*scale)
}
Vector.prototype.mult = function(multVec){
	return new Vector(this.x*multVec.x, this.y*multVec.y);
}}

function Level(numAsteroids){
	this.status = null;
	this.height = canvas.height;
	this.width = canvas.width;
	this.actors = [];
	while(this.actors.length < numAsteroids){
		var randVec = this.genAsteroid();
		var newAsteroid = new Asteroid(asteroidLives, randVec.x, randVec.y, asteroidSpeed);
		if(!this.actorAt(newAsteroid.pos, newAsteroid.size, newAsteroid)){
			this.actors.push(newAsteroid)
		}
	}
}
Level.prototype.actorAt  = function(pos, size, actor){
	
	for(var i = 0; i < this.actors.length; i++){
		var check = this.actors[i]
		if(check != actor&& this.colide(check, pos, size)
		){
			return check;
		}
	}
}
Level.prototype.colide = function(checker, pos, size){
	//returns true if there is a collision with wraparound
	var colided = false;
	var tempCheckXMin = checker.pos.x, tempCheckXMax = checker.pos.x + checker.size.x, tempCheckYMin = checker.pos.y, tempCheckYMax = checker.pos.y + checker.size.y; 
	var tempActXMin = pos.x, tempActXMax = pos.x + size.x, tempActYMin = pos.y, tempActYMax = pos.y + size.y;
	if(tempCheckXMax > this.width){
		tempCheckXMax -= this.width;
		tempCheckXmin -= this.width;
	}
	if(tempCheckYMax > this.height){
		tempCheckYMax -= this.height;
		tempCheckYMin -= this.height;
	}
	if(tempActXMax > this.width){
		tempActXMax -= this.width;
		tempActXMin -= this.width;
	}if (tempActYMax > this.height){
		tempActYMax -= this.height;
		tempActYMin -= this.height;
	}
	//split up or statement, 
	if(pos.x < checker.pos.x+ checker.size.x && pos.x +size.x > checker.pos.x && pos.y < checker.pos.y + checker.size.y && pos.y + size.y > checker.pos.y){
		//normal colision
		colided = true;
	}else if(pos.x < tempCheckXMax && pos.x + size.x > tempCheckXMin && pos.y < tempCheckYMax && pos.y + size.y > tempCheckYMin){
		//normal + tempcheck
		colided = true;
	}else if (tempActXMin < checker.pos.x+ checker.size.x && tempActXMax > checker.pos.x && tempActYMin < checker.pos.y + checker.size.y && tempActYMax > checker.pos.y){
		//tempActor + normal
		colided = true;
	}else if(tempActXMin < tempCheckXMax && tempActXMax > tempCheckXMin && tempActYMin < tempCheckYMax && tempActYMax > tempCheckXMin){
		//tempActor + temp check
		colided = true;
	}
	return colided;
}
Level.prototype.genAsteroid = function(){
	var noGenXMin = this.width/2 -25, noGenYMin = this.height/2 -25, noGenXMax = this.width/2 +25, noGenYMax = this.height/2 +25;
	var potX = Math.random() * this.width;
	while((potX+asteroidSize>noGenXMin && potX<noGenXMax) || potX<asteroidSize || potX +asteroidSize> this.width){
		potX = Math.random() * this.width;
	}
	var potY = Math.random() * this.height;
	while((potY + asteroidSize>noGenYMin && potY<noGenYMax) ||potY<asteroidSize || potY +asteroidSize> this.height){
		potY = Math.random() *this.height;
	}
	return new Vector(potX, potY)
}
Level.prototype.drawActors = function(){
	cx.clearRect(0, 0, this.width, this.height)
	this.actors.forEach(function(actor){
		if(actor.type == "asteroid"){
			lvl.drawAsteroid(actor);
		}
	}, lvl);
}
Level.prototype.drawAsteroid = function(a){
	cx.fillRect(a.pos.x, a.pos.y, a.size.x, a.size.y);
	//all the possible overlaps
	if(a.pos.x + a.size.x>this.width && a.pos.y + a.size.y>this.height){
		//bottom right
		cx.fillRect(a.pos.x - this.width, a.pos.y - this.height, a.size.x, a.size.y)
	}else if(a.pos.x + a.size.x>this.width && a.pos.y < 0){
		//top right
		cx.fillRect(a.pos.x - this.width, a.pos.y + this.height, a.size.x, a.size.y)
	}else if(a.pos.x<0 && a.pos.y + a.size.y>this.height){
		//bottom left
		cx.fillRect(a.pos.x + this.width, a.pos.y - this.height, a.size.x, a.size.y)
	}else if (a.pos.x<0 && a.pos.y< 0){
		//top left
		cx.fillRect(a.pos.x + this.width, a.pos.y + this.height, a.size.x, a.size.y)
	}else if(a.pos.x + a.size.x>this.width){
		//right
		cx.fillRect(a.pos.x - this.width, a.pos.y, a.size.x, a.size.y)
	}else if(a.pos.x<0){
		//left
		cx.fillRect(a.pos.x + this.width, a.pos.y, a.size.x, a.size.y)
	}else if(a.pos.y + a.size.y>this.height){
		//bottom
		cx.fillRect(a.pos.x, a.pos.y - this.height, a.size.x, a.size.y)
	}else if(a.pos.y<0){
		//top
		cx.fillRect(a.pos.x, a.pos.y + this.height, a.size.x, a.size.y)
	}
}
Level.prototype.animate = function(step, keys){
	if(status != null){
	}
	while(step>0){
		this.drawActors();
		//makes sure we are not going faster than maxStep
		if (step>maxStep){
			var currStep = maxStep;
		}else{
			currStep = step;
		}
		for(var i = 0; i < this.actors.length; i++){
			this.actors[i].act(this, currStep, keys);
		}
		step -= currStep;
	}
}

function Asteroid(lives, x, y, speed){
	this.lives = lives;
	this.size = new Vector (asteroidSize*Math.pow(.5, asteroidLives - lives), asteroidSize*Math.pow(.5, asteroidLives - lives))
	this.pos = new Vector(x, y);
	if(speed)
		this.speed = this.genSpeed(speed);
	this.type = "asteroid";
}
Asteroid.prototype.genSpeed = function(asteroidSpeed){
	var randSpeed = Math.random();
	while(randSpeed < .5){
	 randSpeed = Math.random();
	 }
	var speedX = asteroidSpeed*randSpeed;
	var speedY = Math.sqrt(asteroidSpeed*asteroidSpeed*(1-randSpeed*randSpeed));
	if (Math.random() > .5)
		speedX *= -1
	if (Math.random()>.5)
		speedY *=-1
	return new Vector(speedX, speedY);
}
Asteroid.prototype.act = function(level, step){
	var newPos = this.pos.plus(this.speed.scale(step));
	var colider;
	colider = level.actorAt(newPos, this.size, this);
	if (colider){
		if (colider.type = "asteroid"){
			this.asteroidColide(colider);
		}
	}else if(newPos.x>level.width){
    	this.pos.x = 0;
    }else if(newPos.y>level.height){
    	this.pos.y = 0;
    }else if(newPos.x<0){
    	this.pos.x = level.width;
    }else if(newPos.y<0){
    	this.pos.y = level.height;
    }else{
		this.pos = newPos;
	}
}
Asteroid.prototype.split = function(){
}
Asteroid.prototype.asteroidColide = function(otherAsteroid){
	var newSpeed = otherAsteroid.speed;
	otherAsteroid.speed = this.speed;
	this.speed = newSpeed;
}

function animate(level){
	function frame(time) {
		if (lastTime != null)
			updateAnimation(Math.min(100, time - lastTime) / 1000, level);
		lastTime = time;
		requestAnimationFrame(frame);
	}
	requestAnimationFrame(frame);
}
function updateAnimation(step, level){
	level.animate(step);
}

function Ship(x, y){
	this.pos = new Vector(x, y);
}