//Generates a contiguous cave system of arbitrary size. I used it for DnD Campaigns


function BasicDungeon(xSize, ySize, maxRooms, maxCorridors, roomShapes){
	this.xSize = xSize;
	this.ySize = ySize;
	this.maxRooms = maxRooms;
	this.maxCorridors = maxCorridors;
	this.roomShapes = roomShapes;
	this.roomsArray = this.genRooms();
	this.corridorsArray = this.genCorridor();
	this.scoreDungeon();
}
BasicDungeon.prototype.genRooms = function(){
	var rooms = [];
	var length = this.maxRooms * 5;
	
	rooms.push(Math.floor(Math.random() * this.maxRooms/2 + this.maxRooms/2));
	for(var i = 1; i<length; i+=5){
		rooms.push(Math.floor(Math.random() * this.roomShapes.length));
		rooms.push(Math.floor(Math.random() * (this.xSize*2)/(this.maxRooms))+3);
		rooms.push(Math.floor(Math.random() * (this.ySize*2)/(this.maxRooms))+3);
		rooms.push(Math.floor(Math.random() * (this.xSize-rooms[i+1]-1 )));
		rooms.push(Math.floor(Math.random() * (this.ySize-rooms[i+2]-1 )))
	}
	rooms[4] = 0;
	rooms[5] = 0;
	return rooms
	
}
BasicDungeon.prototype.genCorridor = function(){
	var corridors = [];
	var length = this.maxCorridors * 5;
	
	corridors.push(Math.floor(Math.random() *this.maxCorridors)+1);
	for(var i = 1; i<length; i+=5){
		corridors.push(Math.floor(Math.random() * this.xSize));
		corridors.push(Math.floor(Math.random() * this.ySize));
		corridors.push(Math.floor(Math.random() * 3 - 1));
		corridors.push(Math.floor(Math.random() * 3 - 1));
		corridors.push(Math.floor(Math.random() * ((this.xSize+ this.ySize)/4)));
	}
	return corridors
}
	
BasicDungeon.prototype.writeDungeon = function(out){
	var dungeon = [];
	for(var i = 0; i<this.xSize*this.ySize; i++){
		dungeon.push(" ");
	}
	for(var i = 0; i<this.roomsArray[0]; i++){
		dungeon = this.drawRoom(dungeon, i);
	}
	for(var i = 0; i<this.corridorsArray[0]; i++){
		dungeon = this.drawCorridors(dungeon, i);
	}
	if(out){
		console.log(this.corridorsArray[0])
		var temp = [];
		for(var i = 0; i<dungeon.length; i++){
			if(dungeon[i]<0 ){
				temp.push(0);
			}else if (dungeon[i]!==" " ) temp.push(dungeon[i]); else temp.push(" ")
		}
		for(var i = 0; i<this.ySize; i++){
			console.log(i+10, temp.slice(i*this.xSize, (i+1)*this.xSize).join(" "));
		}
	}
	return dungeon;
}
BasicDungeon.prototype.drawRoom = function(dungeon, room){
	var start = this.roomsArray[room*5+4] + this.roomsArray[room*5+5]*this.xSize;
	for(var i = 0; i<this.roomsArray[room*5+3]; i++){
		
		for(var j = 0; j<this.roomsArray[room*5+2]; j++){
			if(dungeon[start + i*this.xSize +j] == " "){
				dungeon[start + i*this.xSize +j] = room+1;
			}
			else{
				dungeon[start + i*this.xSize +j] = -30;
			}
		}
	}
	return dungeon;
}
BasicDungeon.prototype.drawCorridors = function(dungeon, corridor){
	var current = this.corridorsArray[corridor*5+1] + this.corridorsArray[corridor*5+2]*this.xSize;
	for (var i = 0; i<this.corridorsArray[corridor*5+5]; i++){
		if(current > dungeon.length || current < 0){
			return dungeon;
		}
		if(dungeon[current] == " "){
			dungeon[current] = 0;
		}else if(dungeon[current] != 1){
			dungeon[current] = 0;
		}
		current += this.corridorsArray[corridor*5+3];
		if(dungeon[current] == " "){
			dungeon[current] = 0;
		}else if(dungeon[current] != 1){
			dungeon[current] = 0;
		}
		current += this.corridorsArray[corridor*5+4]* this.xSize;
	}
	return dungeon;
}
BasicDungeon.prototype.scoreDungeon = function(){
	var dungeon = this.writeDungeon();
	var score = 0;
	var start = 0;
	var colorArray = [];
	for(var i= 0; i< dungeon.length; i++){
		colorArray.push(" "); 
	}
	recScore(start, this.xSize);
	
	function recScore(start, x){
		colorArray[start] = dungeon[start];
		if(colorArray[start+1] === " " && dungeon[start+1] !== " " && start+1<dungeon.length && start+1%x != 0&&(dungeon[start+1] === dungeon[start] || dungeon[start+1] === 0||dungeon[start] === 0)){
			recScore(start+1, x)
		}
		if(colorArray[start+x] === " " && dungeon[start+x] !== " " && start+x<dungeon.length&&(dungeon[start+x] === dungeon[start] || dungeon[start+x] === 0||dungeon[start] === 0)){
			recScore(start+x, x)
		}
		if(colorArray[start-1] === " " && dungeon[start-1] !== " " && start-1>=0 && start%x != 0&&(dungeon[start-1] === dungeon[start] || dungeon[start-1] === 0||dungeon[start] === 0)){
			recScore(start-1, x)
		}
		if(colorArray[start-x] === " " && dungeon[start-x] !== " " && start-x>=0&&(dungeon[start-x] === dungeon[start] || dungeon[start-x] === 0||dungeon[start] === 0)){
			recScore(start-x, x)
		}
	}
	
	for(var i = 0; i<colorArray.length; i++){
		if(colorArray[i] !== " "&& colorArray[i]>0){
			score++
		}else if(colorArray[i] === 0){
			score--
		}else if(colorArray[i] < 0){
			score+=colorArray[i]
		}
	}
	this.score = score;
	return score;
}
BasicDungeon.prototype.modCorridors = function(){
	var changedIndex = Math.floor(this.corridorsArray.length * Math.random());
	if(changedIndex == 0){
		this.corridorsArray[0]  = Math.floor(Math.random() *this.maxCorridors)+1
	}else if((changedIndex-1)%5 == 0){
		this.corridorsArray[changedIndex] = Math.floor(Math.random() * this.xSize);
	}else if((changedIndex-1)%5 == 1){
		this.corridorsArray[changedIndex] = Math.floor(Math.random() * this.ySize);
	}else if((changedIndex-1)%5 == 2){
		this.corridorsArray[changedIndex] = Math.floor(Math.random() * 3 - 1);
	}else if((changedIndex-1)%5 == 3){
		this.corridorsArray[changedIndex] = Math.floor(Math.random() * 3 - 1);
	}else
		this.corridorsArray[changedIndex] = Math.floor(Math.random() * ((this.xSize+ this.ySize)/4));
}
BasicDungeon.prototype.modRooms = function(){
	var changedIndex = Math.floor(this.roomsArray.length * Math.random());
	if(changedIndex == 0){
		this.roomsArray[0] = Math.floor(Math.random() * this.maxRooms/2 + this.maxRooms/2);
	}else if((changedIndex-1)%5 == 0){
		this.roomsArray[changedIndex] = Math.floor(Math.random() * this.roomShapes.length);
	}else if((changedIndex-1)%5 == 1){
		this.roomsArray[changedIndex] = Math.floor((Math.random() *(this.xSize*2)/this.maxRooms) +3);
	}else if((changedIndex-1)%5 == 2){
		this.roomsArray[changedIndex] = Math.floor((Math.random() * (this.ySize*2)/this.maxRooms) +3);
	}else if((changedIndex-1)%5 == 3){
		this.roomsArray[changedIndex] = Math.floor(Math.random() * (this.xSize-this.roomsArray[changedIndex-2]-1 ));
	}else
		this.roomsArray[changedIndex] = Math.floor(Math.random() * (this.ySize-this.roomsArray[changedIndex-2]-1 ));
}
BasicDungeon.prototype.setCorridors = function(corArray){
	var temp = [];
	for(var i = 0; i< corArray.length; i++){
		temp.push(corArray[i])
	}
	this.corridorsArray = temp;
}
BasicDungeon.prototype.setRooms = function(roomsArray){
	var temp = [];
	for(var i = 0; i<  roomsArray.length; i++){
		temp.push(roomsArray[i])
	}
	this.roomsArray = temp;
}
BasicDungeon.prototype.repro = function(){
	var clone = new BasicDungeon(this.xSize, this.ySize, this.maxRooms, this.maxCorridors, this.roomShapes);
	clone.setRooms(this.roomsArray);
	clone.setCorridors(this.corridorsArray);
	clone.modCorridors();
	clone.modCorridors();
	clone.modCorridors();
	clone.modCorridors();
	clone.modRooms();
	clone.scoreDungeon();
	return clone;
}

function eSABasicDungeon(pop, gens){
	var population = [];
	for(var i = 0; i<pop; i++){
		population.push(new BasicDungeon(100, 50, 10, 15, []));
	}
	for(var i = 0; i< gens; i++){
		population = runGeneration(population)
	}
	var fittest = 0;
	var fittestScore = population[0].score;
	for(var i = 1; i<population.length; i++){
		if(fittestScore < population[i].score){
			fittest = i;
			fittestScore = population[i].score;
		}
	}
	console.log(population[fittest].score)
	population[fittest].writeDungeon(true)
}
function runGeneration(pop){
	var fittest = 0;
	var fittestScore = pop[0].score;
	for(var i = 1; i<pop.length; i++){
		if(fittestScore < pop[i].score){
			fittest = i;
			fittestScore = pop[i].score;
		}
	}
	var newPop = [];
	for(var i = 0; i< pop.length; i++){
		newPop.push(pop[fittest].repro());
	}
	return newPop;
}
//var test = new BasicDungeon(50, 50, 10, 9, [])
//test.writeDungeon(true)
//console.log(test.score)
eSABasicDungeon(200, 40);