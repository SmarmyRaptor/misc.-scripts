function GameID(id){
	this.idNum = id;
	this.playerIds = [];
	this.lastAction = 0;
}

//grid class
function Grid(x, y, players){
	this.gridWidth = x;
	this.gridHeight = y;
	this.grid = this.populateEmptyGrid();
	this.playerLocations = this.populatePlayerMoveGrid(players);
}
{
Grid.prototype.populateEmptyGrid = function(){
//makes an empty grid when the grid class is created
	var returnTrip = [];
	for(var i = 0; i < this.gridWidth * this.gridHeight; i++){
		returnTrip.push(-1);
	}
	return returnTrip;
}
Grid.prototype.populatePlayerMoveGrid = function(players){
	//makes an array full of empty arrays to keep track of player moves.
	var returnArray = [];
	for(var i = 0; i < players; i++){
		returnArray.push([]);
	}
	return returnArray;
}
Grid.prototype.playerMove = function(xLoc, yLoc, player){
	//executes move command within the grid
	var line = this.toLinear(xLoc, yLoc);
	//check to see if the tile is 
	if(xLoc < 0 || xLoc>= this.gridWidth || yLoc < 0 || yLoc>= this.gridHeight){
		console.log("Err: Tile out of bounds");
		return false;
	}
	//checks to make sure it is an empty tile
	else if(this.grid[line] != -1){
		console.log("Err: Tile already occupied")
		return false;
	}
	//checks to make sure it is the proper person's turn;
	else if(this.numMoves()%this.playerLocations.length != player){
		console.log("Err: wrong player");
		return false;
	}
	this.grid[line] = player;
	this.playerLocations[player].push(line);
	return true;
}
Grid.prototype.playerUnmove = function(xLoc, yLoc, player){
	
}
Grid.prototype.toLinear = function(x, y){
	//takes a vector and turns it into a linear coord
	return this.gridWidth*y + x;
}
Grid.prototype.toVector = function(linear){
	//takes a linear coord and returns a vector
	var x = linear%this.width;
	var y = Math.floor(liner/this.width);
	return new Vector(x, y);
}
Grid.prototype.numMoves = function(){
	//counts the number of moves in each player's column
	var sum = 0;
	for(var i = 0; i<this.playerLocations.length; i++){
		sum += this.playerLocations[i].length;
	}
	return sum;
}
}

function Moves(){
	//keeps track of where the move counter is
	this.movePointer = 0;
	this.moveNumber = 0;
	this.moveArray = [];

}
var commands = {
	//place a piece
	move:function(x, y, player, hipgame){
		//if it is a legal move, do shit.
		if(hipgame.gameGrid.playerMove(x, y, player)){
			hipgame.moveTrack.moveArray.push({
				type:"move",
				player:player,
				x: x,
				y: y
			})
			
			hipgame.moveTrack.moveNumber++;
			hipgame.moveTrack.movePointer++;
			//get rid of all the moves after the pointer
			for(var i = hipgame.moveTrack.movePointer; i<moveNumber; i++){
				hipgame.moveTrack.moveArray.pop();
			}
		}
	}
	unmove:function(){
		
	}
}

function HipGame(id, width, height, players){
	this.identification = new GameID(id);
	this.moveTrack = new Moves(this);
	this.gameGrid = new Grid(width, height, players);
	this.commands = commands
}



HipGame.prototype.printGame = function(){
	this.gameGrid.gridWidth;
	for(var i = 0; i < this.gameGrid.gridHeight; i++){
		var printLine = "- ";
		for(var j = i*this.gameGrid.gridWidth; j < (i+1)*this.gameGrid.gridWidth; j++){
			if(this.gameGrid.grid[j] != -1)
				printLine += this.gameGrid.grid[j]+" ";
			else
				printLine += "X ";
		}
		printLine += "-"
		console.log(printLine);
	}
	return true;
}

{function Vector(x, y){
	this.x = x;
	this.y = y;
}
Vector.prototype.plus = function(plusVector){
	return new Vector(this.x + plusVector.x, this.y + plusVector.y)
}
Vector.prototype.scale = function(scale){
	return new Vector(this.x*scale, this.y*scale)
}
Vector.prototype.mult = function(multVec){
	return new Vector(this.x*multVec.x, this.y*multVec.y);
}}

var test = new HipGame("pop", 6, 6, 2);
console.log(test);
test.printGame();
