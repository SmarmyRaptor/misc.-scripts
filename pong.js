var ballSpeed = 200
var ballAccel = 10;
var paddleSize = 60;
var paddleSpeed = 400;
var paddleInfluence = 70;
var maxStep = .02;
var levelPlan = {
	balls: 1,
	players: [[87, 83], [38, 40]],
	height: 400,
	width: 600
}
function elt(name, className){
	var elt = document.createElement(name);
	if(className)
		elt.className = className;
	return elt;
}
function Display(parent, level){
	this.wrap = parent.appendChild(elt("div", "game"));
	this.level = level;
	
	this.wrap.appendChild(this.drawBackground());
	this.actorLayer = null;
	this.drawFrame();
}
Display.prototype.drawBackground = function(){
	var box = elt("div", "background");
	box.style.width = this.level.width;
	box.style.height = this.level.height
	this.level.walls.forEach(function(wall){
		var wallDiv = elt("div", wall.type);
			wallDiv.style.left =  wall.pos.x+"px";
			wallDiv.style.top = wall.pos.y+"px";
			wallDiv.style.width = wall.size.x+"px";
			wallDiv.style.height = wall.size.y + "px"
			box.appendChild(wallDiv);
		}
	)
	return box;
}
Display.prototype.drawActors = function(){
	var wrap = elt("div");
	this.level.actors.forEach(function(actor){
		var actorDiv = elt("div", actor.type);
		actorDiv.style.left = actor.pos.x +"px";
		actorDiv.style.top = actor.pos.y +"px";
		actorDiv.style.width = actor.size.x +"px";
		actorDiv.style.height = actor.size.y +"px";
		wrap.appendChild(actorDiv);
	});
	return wrap;
};
Display.prototype.drawFrame = function(){
	if(this.actorLayer)
		this.wrap.removeChild(this.actorLayer)
	this.actorLayer = this.drawActors();
	this.wrap.appendChild(this.actorLayer);
};

function Vector(x, y){
	this.x = x;
	this.y = y;
}
Vector.prototype.plus = function(plusVec){
	return new Vector(this.x + plusVec.x, this.y+plusVec.y)
}
Vector.prototype.times = function(plusVec){
	return new Vector(this.x * plusVec.x, this.y * plusVec.y)
}
Vector.prototype.length = function(){
	return Math.sqrt(this.x*this.x + this.y*this.y);
}


function Level(levelPlan){
	this.height = levelPlan.height;
	this.width = levelPlan.width;
	this.scoreLeft = 0;
	this.scoreRight = 0;
	
	this.actors = [];
	this.walls = [];
	//place balls
	for(var i = 0; i<levelPlan.balls; i++){
		this.actors.push(new Ball(this.height/2, this.width/2));
	}
	//place top and bottom walls
	this.walls.push(new sideWall(10, this.width, "horiz"));
	this.walls.push(new sideWall(this.height - 10, this.width, "horiz"));
	//place paddles
	this.actors.push(new Player(levelPlan.players[0], 20, this.height));
	this.actors.push(new Player(levelPlan.players[1], this.width-20, this.height));
	
	this.status = null;
};
Level.prototype.wallAt = function(pos, size){
	var xMin = pos.x;
    var xMax = pos.x + size.x;
	var yMin = pos.y;
    var yMax = pos.y + size.y;
	//check to see if scored
	if(xMin > this.width){
		return "pointLeft"
	}else if(xMax < 0){
		return "pointRight"
	}
	//check to see if it is hitting any walls
  	var isWall = false; 
	this.walls.forEach(function(wall){
		if( xMin<wall.pos.x + wall.size.x && xMax>wall.pos.x && yMin < wall.pos.y + wall.size.y && yMax > wall.pos.y){
          isWall= wall.orientation;
		}
	})
    return isWall;
}
Level.prototype.actorAt = function(pos, size, actor){
	var xMin = pos.x, xMax = pos.x+ size.x;
	var yMin = pos.y, yMax = pos.y + size.y;
	
	for(var i = 0; i< this.actors.length; i++){
		var check = this.actors[i];
		if(check != actor && check.type != "ball" && xMin<check.pos.x + check.size.x && xMax>check.pos.x && yMin < check.pos.y + check.size.y && yMax > check.pos.y){
			check.hitAtY = ((pos.y+size.y/2)-check.pos.y)/check.size.y -.5
			check.hitAtX =  ((pos.x+size.x/2)-check.pos.x)/check.size.x -.5
			return check;
		}
		
	}
}
Level.prototype.animate = function(step, keys){
	if(status != null){
	}
	while(step>0){
		//makes sure we are not going faster than maxStep
		if (step>maxStep){
			var currStep = maxStep;
		}else{
			currStep = step;
		}
		for(var i = 0; i < this.actors.length; i++){
			this.actors[i].act(this, currStep, keys);
		}
		step -= currStep;
	}
}

function sideWall(yPos, width, orientation){
	this.pos = new Vector(10, yPos);
	this.size = new Vector(width-20, 5);
	this.type = "sideWall";
	this.orientation = orientation;
}

function Ball(yPos, xPos){
	this.startPos = new Vector(xPos-5, yPos-5);
	this.pos = new Vector(xPos-5, yPos-5);
	this.size = new Vector(10, 10);
	this.paddleColide = false;
	//generate a random vector with speed = ballSpeed

	this.speed = this.genSpeed();
	this.type = "ball";
}
Ball.prototype.genSpeed = function(){
	var randSpeed = Math.random();
	while(randSpeed < .5){
	 randSpeed = Math.random();
	 }
	var speedX = ballSpeed*randSpeed;
	var speedY = Math.sqrt(ballSpeed*ballSpeed*(1-randSpeed*randSpeed));
	if (Math.random() > .5)
		speedX *= -1
	if (Math.random()>.5)
		speedY *=-1
	return new Vector(speedX, speedY);
}
Ball.prototype.act = function(level, step){
	var newPos = this.pos.plus(new Vector(this.speed.x*step, this.speed.y*step));
	var wallColide = level.wallAt(newPos, this.size);
	var actorColide = level.actorAt(newPos, this.size, this);
	if (wallColide){
		if (wallColide == "pointLeft"){
			this.pos = this.startPos;
			this.speed = this.genSpeed();
			level.scoreRight++;
		}else if(wallColide == "pointRight"){
			this.pos = this.startPos;
			this.speed = this.genSpeed();
			level.scoreleft++;
		}else if(wallColide == "horiz"){
			this.speed = this.speed.times(new Vector(1, -1))
		}else if(wallColide == "vert"){
			this.speed = this.speed.times(new Vector(-1, 1))
		}
    }else if(actorColide){
		if(actorColide.type == "paddle" && !this.paddleColide){
			this.speed = this.speed.times(new Vector(-1, 1));
			this.speed = this.speed.plus(new Vector(0, paddleInfluence*actorColide.hitAtY))
			this.paddleColide = true;
			}
		else{
			this.pos = newPos;
			this.paddleColide = false;
			if (this.speed.x>0)
				this.speed = this.speed.plus(new Vector(ballAccel*step, 0))
			else
				this.speed = this.speed.plus(new Vector(-1*ballAccel*step, 0))
		}
	}else{
		this.pos = newPos;
		if (this.speed.x>0)
			this.speed = this.speed.plus(new Vector(ballAccel*step, 0))
		else
			this.speed = this.speed.plus(new Vector(-1*ballAccel*step, 0))
		this.paddleColide = false
	}
}

function Player(control, xPos, height){
	this.pos = new Vector(xPos, height/2 - paddleSize/2);
	this.size = new Vector(5, paddleSize);
	this.speed = new Vector(0, 0);
	this.type = "paddle"
	this.orientation = "vert"
	this.up = control[0];
	this.down = control[1];
}
Player.prototype.act = function(level, step, keys){
	this.speed.y = 0;
	if(keys[this.up])
		this.speed.y -= paddleSpeed;
	if(keys[this.down])
		this.speed.y += paddleSpeed;
	var newPos = this.pos.plus(new Vector(this.speed.x*step, this.speed.y*step));
	var wallColide = level.wallAt(newPos, this.size);
	var actorColide = level.actorAt(newPos, this.size, this);
	if (!wallColide)
		this.pos = newPos;
	
} 


function trackKeys(){
	var pressed = Object.create(null);
	function handler(event){
		var down = event.type == "keydown";
		pressed[event.keyCode] = down;
		event.preventDefault();
	}
	addEventListener("keydown", handler);
	addEventListener("keyup", handler);
	return pressed;
}

var keys = trackKeys();
function runAnimation(frameFunc) {
  var lastTime = null;
  function frame(time) {
    var stop = false;
    if (lastTime != null) {
      var timeStep = Math.min(time - lastTime, 100) / 1000;
      stop = frameFunc(timeStep) === false;
    }
    lastTime = time;
    if (!stop)
      requestAnimationFrame(frame);
  }
  requestAnimationFrame(frame);
}
function runLevel(level) {
  var display = new Display(document.body, level);
  runAnimation(function(step) {
    level.animate(step, keys);
    display.drawFrame(step);
    
  });
}


var level = new Level(levelPlan);
runLevel(level);