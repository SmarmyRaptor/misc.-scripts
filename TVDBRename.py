import os
import argparse

#Directory ShowName Opt: season split
parser = argparse.ArgumentParser()
parser.add_argument("dir")
parser.add_argument("showname")
parser.add_argument("--seasonFolders", "-f", action='store_true', help="Episodes are already sorted into folders by season")
parser.add_argument("--seasonSplit", "-s", nargs='+', help = "Split the episodes into seasons of length x y z ")
parser.add_argument("--seasonDigits", "-d", default = 2, type = int)
parser.add_argument("--episodeDigits", "-e", default = 2, type = int)

args = parser.parse_args()


def renameSeason(dir, season, name):
	working_dir = args.dir+'/'+dir+'/'
	dir_content = os.listdir(working_dir)
	dir_content.sort()
	loc = 1;
	for episode in dir_content:
		if os.path.isfile(working_dir+episode):
			ext = os.path.splitext(working_dir+episode)
			new_name = name + " S" + str(season).zfill(args.seasonDigits) + "E" + str(loc).zfill(args.episodeDigits)+ext[1]
			os.rename(working_dir+episode, working_dir+new_name)
			loc = loc + 1

def makeSeasonFolder(folderName, episodes):
	dir_content = os.listdir(args.dir)
	os.mkdir(args.dir+'/'+folderName)

	count = 0
	for episode in dir_content:
		if os.path.isfile(args.dir+'/'+str(episode)) and count < int(episodes):
			os.rename(args.dir+'/'+episode, args.dir+'/'+folderName+'/'+episode)
			count = count + 1

def seasonSplit():
	season_num = 1;
	while os.path.isdir(args.dir+'/'+"Season "+str(season_num)):
		##Rename them
		renameSeason("Season "+str(season_num), season_num, args.showname)
		print(os.listdir(args.dir+'/'+"Season "+str(season_num)))
		season_num = season_num + 1
	


	print (args.seasonSplit)


##Auto Split seasons	
if args.seasonSplit:
	season_num = 1;
	for length in args.seasonSplit:
		makeSeasonFolder("Season "+str(season_num), length)
		season_num = season_num + 1
	
	##makes the season folders
	seasonSplit()
	
##Episodes pre split
elif args.seasonFolders:
	seasonSplit()

##All one Season
else:
	renameSeason("", 1, args.showname)
